module.exports = {
  purge: [
      './public/index.html',
  ],
  theme: {
    extend: {
      screens: {
        'dark': {'raw': '(prefers-color-scheme: dark)'},
      }
    },
  },
  variants: {
    textColor: ['responsive', 'visited'],
  },
  plugins: [],
}
